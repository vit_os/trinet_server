<?php
/**
 * Created by PhpStorm.
 * User: vit
 * Date: 08.08.19
 * Time: 13:14
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Connection;

use App\Entity\User;


class CreateUserCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-user';

    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        parent::__construct();
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        for ($i = 0; $i < 10000; $i++) {
            $this->connection->executeQuery('insert into user (`login`, `email`, `update_dt`, `create_dt`) 
                                                  VALUES("login-' . $i . '", "email.' . $i . '@mail.ru","' . date('Y-m-d H:i:s') . '", "' . date('Y-m-d H:i:s') . '")');

        }
    }
}