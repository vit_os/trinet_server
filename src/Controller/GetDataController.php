<?php

namespace App\Controller;

use function Sodium\version_string;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;


class GetDataController extends AbstractController
{
    private $salt        = 'salt_is_salt';
    private $session;
    private $itemsOnPage = 100;



    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/login", name="login")
     */
    public function index()
    {
        $hash = hash('md5', 'user' . date('Y-m-d') . $this->salt);
        $this->login($hash);

        return $this->json(['message' => 'Ok', 'url' => '/get/numberpages', 'page' => 0,]);
    }

    /**
     * @Route("/get/users", name="get_users")
     */

    public function getUsers()
    {
        $this->checkAuth();
        $request = Request::createFromGlobals();

        $date    = $request->query->get('date');
        $page    = (int)$request->query->get('page');
        $items   = $request->query->get('items');
        $date    = str_replace('_', ' ', $date);

        $repository = $this->getDoctrine()->getRepository(User::class);
        $users      = $repository->findByUpdateDT($date, $this->itemsOnPage * $page, $items);

        $output = [];
        foreach ($users as $user) {
            $userId                       = $user->getId();
            $output[$userId]['id']        = $userId;
            $output[$userId]['update_dt'] = $user->getUpdateDt()->getTimestamp();
            $output[$userId]['create_dt'] = $user->getCreateDt()->getTimestamp();
            $output[$userId]['login']     = $user->getLogin();
            $output[$userId]['email']     = $user->getEmail();

        }

        $jsonData = ['data' => $output];



        return $this->json($jsonData);
    }

    /**
     * @Route("/get/numberpages", name="get_number_pages")
     */

    public function getNumberOfPages()
    {
        $this->checkAuth();
        $request = Request::createFromGlobals();
        $date    = $request->query->get('date');
        $repository = $this->getDoctrine()->getRepository(User::class);
        $rows      = $repository->getCountByUpdateDT($date);
        $count = array_shift($rows);

        $number   = (int)ceil($count / $this->itemsOnPage);
        return $this->json(['number_pages'=>$number,'url' => '/get/users']);
    }

    private function setPageNotFound(){
        header("HTTP/1.0 404 Not Found");
        exit();
    }

    /**
     * @param string $hash
     * проверяем совпадает ли hash с тем который у нас если да то пишем в сессию
     */
    private function login(string $hash)
    {

        $request = Request::createFromGlobals();
        $key = $request->request->get('key');
        if (!isset($key) || $hash != $key) {
            $this->setPageNotFound();
        }
        if (isset($key) && $hash != $key) {
            $this->setPageNotFound();
        }
        $this->session->set('key', $hash);


    }

    private function checkAuth()
    {
        $request = Request::createFromGlobals();
        $key = $request->request->get('key');
        $sessionHash = $this->session->get('key');
        if ($sessionHash !== $key) {
            $this->setPageNotFound();
        }
        return true;
    }

}
