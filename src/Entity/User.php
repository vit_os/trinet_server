<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_dt;

    /**
     * ORM\Table(indexes={@ORM\Index(name="update_dt_idx", columns={"update_dt"})})
     * @ORM\Column(type="datetime")
     */
    private $update_dt;

    /**
     * @ORM\Column( name="email" , unique=true,  length=255)
     */
    private $email;

    /**
     * @ORM\Column( name="login" , unique=true,  length=255)
     */
    private $login;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreateDt(): ?\DateTimeInterface
    {
        return $this->create_dt;
    }

    public function setCreateDt(\DateTimeInterface $create_dt): self
    {
        $this->create_dt = $create_dt;

        return $this;
    }

    public function getUpdateDt(): ?\DateTimeInterface
    {
        return $this->update_dt;
    }

    public function setUpdateDt(\DateTimeInterface $update_dt): self
    {
        $this->update_dt = $update_dt;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }
}
